'use strict';
const BuildingRepository = require('../repository/buildingrepository');
const buildingRepository = new BuildingRepository();

class BuildingService {
    constructor() {}
    addBuilding(building, callback) {
        buildingRepository.addBuilding(building, (error, Building) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Building);
            }
        });
    }
    getBuilding(buildingId, callback) {
        buildingRepository.getBuilding(buildingId, (error, Building) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Building);
            }
        });
    }
    deleteBuilding(buildingId, officeId, callback) {
        buildingRepository.deleteBuilding(buildingId, officeId, (error, Building) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Building);
            }
        });
    }
    updateBuilding(building, callback) {
        buildingRepository.updateBuilding(building, (error, Building) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Building);
            }
        });
    };
}
module.exports = BuildingService;