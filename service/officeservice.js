'use strict';
const OfficeRepository = require('../repository/officerepository');
const officeRepository = new OfficeRepository();

class OfficeService {
    constructor() {}
    addOffice(office, callback) {
        officeRepository.addOffice(office, (error, Office) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Office);
            }
        });
    }
    getOffice(officeId, callback) {
        officeRepository.getOffice(officeId, (error, Office) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Office);
            }
        });
    }
    deleteOffice(officeId, callback) {
        officeRepository.deleteOffice(officeId, (error, Office) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Office);
            }
        });
    }
    updateOffice(officeId, office, callback) {
        officeRepository.updateOffice(officeId, office, (error, Office) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Office);
            }
        });
    }
}

module.exports = OfficeService;