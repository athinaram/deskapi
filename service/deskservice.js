'use strict';
const DeskRepository = require('../repository/deskrepository');
const deskRepository = new DeskRepository();

class DeskService {
    constructor() {}
    adddesk(desk, callback) {
        deskRepository.addDesk(desk, (error, desk) => {
            if (error) {
                callback(error);
            } else {
                callback(null, desk);
            }
        });
    }
    getdesk(deskId, callback) {
        deskRepository.getDesk(deskId, (error, desk) => {
            if (error) {
                callback(error);
            } else {
                callback(null, desk);
            }
        });
    }
    deletedesk(deskId, callback) {
        deskRepository.deleteDesk(deskId, (error, desk) => {
            if (error) {
                callback(error);
            } else {
                callback(null, desk);
            }
        });
    }
    updatedesk(desk, callback) {
        deskRepository.updateDesk(desk, (error, desk) => {
            if (error) {
                callback(error);
            } else {
                callback(null, desk);
            }
        });
    }
}

module.exports = DeskService;