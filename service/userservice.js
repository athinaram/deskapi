'use strict';
const UserRepository = require('../repository/userrepository');
const userRepository = new UserRepository();

class UserService {
    constructor() {}
    addUser(user, callback) {
        userRepository.addUser(user, (error, User) => {
            if (error) {
                callback(error);
            } else {
                callback(null, User);
            }
        });
    }
    getUser(userId, callback) {
        userRepository.getUser(userId, (error, User) => {
            if (error) {
                callback(error);
            } else {
                callback(null, User);
            }
        });
    }
    deleteUser(userId, callback) {
        userRepository.deleteUser(userId, (error, User) => {
            if (error) {
                callback(error);
            } else {
                callback(null, User);
            }
        });
    }
    updateUser(user, callback) {
        userRepository.updateUser(user, (error, User) => {
            if (error) {
                callback(error);
            } else {
                callback(null, User);
            }
        });
    }
}

module.exports = UserService;