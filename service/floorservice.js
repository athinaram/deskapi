'use strict';
const FloorRepository = require('../repository/floorrepository');
const floorRepository = new FloorRepository();

class FloorService {
    constructor() {}
    addFloor(floor, callback) {
        floorRepository.addFloor(floor, (error, Floor) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Floor);
            }
        });
    }
    getFloor(floorId, callback) {
        floorRepository.getFloor(floorId, (error, Floor) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Floor);
            }
        });
    }
    deleteFloor(floorId, callback) {
        floorRepository.deleteFloor(floorId, (error, Floor) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Floor);
            }
        });
    }
    updateFloor(floor, callback) {
        floorRepository.updateFloor(floor, (error, Floor) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Floor);
            }
        });
    };
}

module.exports = FloorService;