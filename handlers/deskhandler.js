'use strict';

const Constants = require('../constants/constants');

const DeskService = require('../service/DeskService');
const deskService = new DeskService();

const addDesk = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    if (event.body) {
        let desk = event.body;
        deskService.addDesk(desk, (error, Desk) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Desk);
            }
        });
    } else {
        callback(Constants.EMPTY_BODY);
    }
};

const getDesk = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let deskId = event.pathParameters.deskId;
    if (deskId) {
        deskService.getDesk(deskId, (error, Desk) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Desk);
            }
        });
    }else {
        callback(deskId + Constants.EMPTY);
    }
};

const deleteDesk = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let deskId = event.pathParameters.deskId;
    if (deskId) {
        deskService.deleteDesk(deskId, (error, Desk) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Desk);
            }
        });
    }else {
        callback(deskId + Constants.EMPTY);
    }
};
const updateDesk = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let deskId = event.pathParameters.deskId;
    if (deskId) {
        deskService.updateDesk(deskId, (error, Desk) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Desk);
            }
        });
    }else {
        callback(deskId + Constants.EMPTY);
    }
};

module.exports = {
    addDesk: addDesk,
    getDesk: getDesk,
    deleteDesk: deleteDesk,
    updateDesk: updateDesk
}