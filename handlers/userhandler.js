'use strict';
const Constants = require('../constants/constants');

const UserService = require('../service/userservice');
const userService = new UserService();

const addUser = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    if (event.body) {
        let user = event.body;
        userService.addUser(user, (error, user) => {
            if (error) {
                callback(error);
            } else {
                callback(null, user);
            }
        });
    } else {
        callback(Constants.EMPTY_BODY);
    }
};

const getUser = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let userId = event.pathParameters.userId;
    if (userId) {
        userService.getUser(userId, (error, user) => {
            if (error) {
                callback(error);
            } else {
                callback(null, user);
            }
        });
    } else {
        callback(userId + Constants.EMPTY);
    }
};

const deleteUser = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let userId = event.pathParameters.userId;
    if (userId) {
        userService.deleteUser(userId, (error, user) => {
            if (error) {
                callback(error);
            } else {
                callback(null, user);
            }
        });
    } else {
        callback(userId + Constants.EMPTY);
    }
};

const updateUser = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let userId = event.pathParameters.userId;
    if (userId) {
        userService.updateUser(userId, (error, user) => {
            if (error) {
                callback(error);
            } else {
                callback(null, user);
            }
        });
    } else {
        callback(userId + Constants.EMPTY);
    }
};

module.exports = {
    addUser: addUser,
    getUser: getUser,
    deleteUser: deleteUser,
    updateUser: updateUser
}