'use strict';
const Constants = require('../constants/constants');

const FloorService = require('../service/Floorservice');
const floorService = new FloorService();

const addFloor = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    if (event.body) {
        floorService.addFloor(Floor, (error, Floor) => {
            if (error) {
                callback(error);
            } else {
                callback(null, building);
            }
        });
    } else {
        callback(Constants.EMPTY_BODY);
    }
};

const getFloor = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let floorId = event.pathParameters.floorId;
    if (floorId) {
        floorService.getFloor(FloorId, (error, Floor) => {
            if (error) {
                callback(error);
            } else {
                callback(null, building);
            }
        });
    }else {
        callback(floorId + Constants.EMPTY);
    }
};

const deleteFloor = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let floorId = event.pathParameters.floorId;
    if (floorId) {
        floorService.deleteFloor(FloorId, (error, Floor) => {
            if (error) {
                callback(error);
            } else {
                callback(null, building);
            }
        });
    }else {
        callback(floorId + Constants.EMPTY);
    }
};
const updateFloor = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let floorId = event.pathParameters.floorId;
    if (floorId) {
        floorService.updateFloor(FloorId, (error, Floor) => {
            if (error) {
                callback(error);
            } else {
                callback(null, building);
            }
        });
    }else {
        callback(floorId + Constants.EMPTY);
    }
};

module.exports = {
    addFloor: addFloor,
    getFloor: getFloor,
    deleteFloor: deleteFloor,
    updateFloor: updateFloor
}
