'use strict';
const Constants = require('../constants/constants');

const BuildingService = require('../service/Buildingservice');
const buildingService = new BuildingService();

const addBuilding = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    if (event.body) {
        let building = event.body;
        buildingService.addBuilding(building, (error, Building) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Building);
            }
        });
    } else {
        callback(Constants.EMPTY_BODY);
    }
};

const getBuilding = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let buildingId = event.pathParameters.buildingId;
    if (buildingId) {
        buildingService.getBuilding(buildingId, (error, Building) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Building);
            }
        });
    } else {
        callback(buildingId + Constants.EMPTY);
    }
};

const deleteBuilding = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let buildingId = event.pathParameters.buildingId;
    let officeId = event.pathParameters.officeId;
    if (buildingId) {
        buildingService.deleteBuilding(buildingId, officeId, (error, Building) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Building);
            }
        });
    } else {
        callback(buildingId + Constants.EMPTY);
    }
};
const updateBuilding = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let buildingId = event.pathParameters.buildingId;
    if (buildingId) {
        buildingService.updateBuilding(buildingId, (error, Building) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Building);
            }
        });
    } else {
        callback(buildingId + Constants.EMPTY);
    }
};

module.exports = {
    addBuilding: addBuilding,
    getBuilding: getBuilding,
    deleteBuilding: deleteBuilding,
    updateBuilding: updateBuilding
}