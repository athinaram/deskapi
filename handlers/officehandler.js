'use strict';
const Constants = require('../constants/constants');

const officeService = require('../service/officeservice');
const OfficeService = new officeService();

const addOffice = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    if (event.body) {
        let office = event.body;
        OfficeService.addOffice(office, (error, Office) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Office);
            }
        });
    } else {
        callback(Constants.EMPTY_BODY);
    }
};

const getOffice = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let officeId = event.pathParameters.officeId;
    if (officeId) {
        OfficeService.getOffice(officeId, (error, Office) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Office);
            }
        });
    } else {
        callback(officeId + Constants.EMPTY);
    }
};

const deleteOffice = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let officeId = event.pathParameters.officeId;
    if (officeId) {
        OfficeService.deleteOffice(officeId, (error, Office) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Office);
            }
        });
    } else {
        callback(officeId + Constants.EMPTY);
    }
};
const updateOffice = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    let officeId = event.pathParameters.officeId;
    let office = event.body;
    if (officeId && office) {
        OfficeService.updateOffice(officeId, office,  (error, Office) => {
            if (error) {
                callback(error);
            } else {
                callback(null, Office);
            }
        });
    } else {
        callback(officeId + Constants.EMPTY);
    }
};


module.exports = {
    addOffice: addOffice,
    getOffice: getOffice,
    deleteOffice: deleteOffice,
    updateOffice: updateOffice
}