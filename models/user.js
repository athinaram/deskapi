/**
 * Dynamoose model for User
 * @Version : v1.0
 */

var dynamoose = require('dynamoose');

/**
 * Schema User
 * @type {dynamoose}
 */
const userSchema = new dynamoose.Schema({
  'userId': {
    type: String
  },
  'userType': {
    type: String,
    enum: ['user', 'superuser']
  },
  'userName': {
    type: String
  },
  'officeId': {
    type: String
  },
  'buildingId': {
    type: String
  },
  'floorId': {
    type: String
  },
  'deskId': {
    type: String
  },
  'createdBy': {
    type: String
  },
  'availabilityStartDate': {
    type: String
  },
  'availabilityEndDate': {
    type: String
  },
  'createdBy': {
    type: String
  },
  'createdDate': {
    type: Date,
    default: Date.now()
  },
  'updatedBy': {
    type: String
  },
  'updatedDate': {
    type: Date,
    default: Date.now()
  }
}, {
  'strict': false
});

function getUserModel() {
  if (dynamoose.models && dynamoose.models.user) {
    return dynamoose.models.user;
  } else {
    return dynamoose.model('user', userSchema);
  }
}

module.exports = getUserModel();