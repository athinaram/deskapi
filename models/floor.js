/**
 * Dynamoose model for Floor
 * @Version : v1.0
 */

var dynamoose = require('dynamoose');

/**
 * Schema Floor
 * @type {dynamoose}
 */
const floorSchema = new dynamoose.Schema({
  'floorId': {
    type: String
  },
  'buildingId': {
    type: String
  },
  'officeId': {
    type: String
  },
  'desks': {
    type: Number
  },
  'createdBy': {
    type: String
  },
  'createdDate': {
    type: Date,
    default: Date.now()
  },
  'updatedBy': {
    type: String
  },
  'updatedDate': {
    type: Date,
    default: Date.now()
  }
}, {
  'strict': false
});

function getFloorModel() {
  if (dynamoose.models && dynamoose.models.floor) {
    return dynamoose.models.floor;
  } else {
    return dynamoose.model('floor', floorSchema);
  }
}

module.exports = getFloorModel();