/**
 * Dynamoose model for Desk
 * @Version : v1.0
 */

var dynamoose = require('dynamoose');

/**
 * Schema Desk
 * @type {dynamoose}
 */
const deskSchema = new dynamoose.Schema({
  'deskId': {
    type: String
  },
  'floorId': {
    type: String
  },
  'buildingId': {
    type: String
  },
  'officeId': {
    type: String
  },
  'userId': {
    type: String
  },
  'deskNumber': {
    type: String
  },
  'section': {
    type: String
  },
  'available': {
    type: Boolean,
    enum: [true, false]
  },
  'availabilityStartDate': {
    type: String
  },
  'availabilityEndDate': {
    type: String
  },
  'temporarilyAssigned': {
    type: String
  },
  'notes': {
    type: String
  },
  'createdBy': {
    type: String
  },
  'createdDate': {
    type: Date,
    default: Date.now()
  },
  'updatedBy': {
    type: String
  },
  'updatedDate': {
    type: Date,
    default: Date.now()
  }
}, {
  'strict': false
});

function getDeskModel() {
  if (dynamoose.models && dynamoose.models.desk) {
    return dynamoose.models.desk;
  } else {
    return dynamoose.model('desk', deskSchema);
  }
}

module.exports = getDeskModel();