/**
 * Dynamoose model for Building
 * @Version : v1.0
 */

var dynamoose = require('dynamoose');

/**
 * Schema Building
 * @type {dynamoose}
 */
const buildingSchema = new dynamoose.Schema({
  'buildingId': {
    type: String
  },
  'officeId': {
    type: String
  },
  'floors': {
    type: Number
  },
  'address': {
    type: String
  },
  'createdBy': {
    type: String
  },
  'createdDate': {
    type: Date,
    default: Date.now()
  },
  'updatedBy': {
    type: String
  },
  'updatedDate': {
    type: Date,
    default: Date.now()
  }
}, {
  'strict': false
});

function getBuildingModel() {
  if (dynamoose.models && dynamoose.models.building) {
    return dynamoose.models.building;
  } else {
    return dynamoose.model('building', buildingSchema);
  }
}

module.exports = getBuildingModel();