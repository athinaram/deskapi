/**
 * Dynamoose model for Office
 * @Version : v1.0
 */

var dynamoose = require('dynamoose');

/**
 * Schema Office
 * @type {dynamoose}
 */
const officeSchema = new dynamoose.Schema({
  'officeId': {
    type: String
  },
  'officeName': {
    type: String
  },
  'address': {
    type: String
  },
  'buildings': {
    type: Number
  },
  'createdBy': {
    type: String
  },
  'createdDate': {
    type: Date,
    default: Date.now()
  },
  'updatedBy': {
    type: String
  },
  'updatedDate': {
    type: Date,
    default: Date.now()
  }
}, {
    'strict': false
});

function getOfficeModel() {
    if (dynamoose.models && dynamoose.models.office) {
        return dynamoose.models.office;
    } else {
        return dynamoose.model('office', officeSchema);
    }
}

module.exports = getOfficeModel();
