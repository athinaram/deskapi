'use strict';
/**
 * 
 */
var AWS = require("aws-sdk");
var dynamoose = require('dynamoose');
const Constants = require('../constants/constants');

dynamoose.AWS.config.update({
	region: Constants.REGION
});

const BuildingModel = require('../models/building');
const uniquebuildingId = require('uuid/v1');

const officeRepository = require('../repository/officerepository');

class BuildingRepository {
	constructor() {}

	addBuilding(building, callback) {
		building.buildingId = uniquebuildingId();
		BuildingModel.create(building, function (err, building) {
			if (err) {
				 callback(err);
			}
			let increment = true;
			officeRepository.updateOfficeBuildingCount(building.officeId, increment);
			 callback(null, building);
		});
	}

getBuilding(buildingId, callback) {
	BuildingModel.scan(Constants.building_ID).eq(buildingId).exec(function (err, building) {
		if (err) {
			 callback(err);
		}
		 callback(null, building);
	});
}

updateBuilding(buildingId, building, callback) {
	//Update needs to be modified according to the changes that are going to be updated

	BuildingModel.update({
		'buildingId': buildingId
	}, {
		$PUT: {
			'address': building.address,
			'floors': building.floors,
			'address': building.address,
			'updatedBy': building.updatedBy
		}
	}, function (err, building) {
		if (err) {
			 callback(err);
		}
		 callback(null, building);
	});
}

updateBuildingFloorsCount(buildingId, increment, callback) {
	let buildingList = this.getBuilding(buildingId);
	let building = buildingList[0];
	let floorCount = building.floors;
	if (increment) {
		floorCount++;
	} else {
		floorCount--;
	}
	//Updating the building count when adding or deleting building for office
	BuildingModel.update({
		'buildingId': officeId
	}, {
		$PUT: {
			'floors': floorCount
		}
	}, function (err, building) {
		if (err) {
			 callback(err);
		}
		 callback(null, building);
	});
}

deleteBuilding(buildingId, officeId, callback) {
	BuildingModel.delete(buildingId, function (err, building) {
		if (err) {
			callback(err);
		}
		let decrement = false;
		officeRepository.updateOfficeBuildingCount(officeId, decrement);
		callback(null, building);
	});
}

deleteBuildingsByOfficeId(officeId, callback) {
	BuildingModel.delete(officeId, function (err, buildings) {
		if (err) {
			callback(err);
		}
		let increment = false;
		officeRepository.updateOfficeBuildingCount(building.officeId, increment);
		callback(null, buildings);
	});
}
}

module.exports = BuildingRepository;