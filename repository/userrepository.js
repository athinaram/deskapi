'use strict';
/**
 * 
 */
var AWS = require("aws-sdk");
var dynamoose = require('dynamoose');
var Constants = require('../constants/constants');

dynamoose.AWS.config.update({
	region: Constants.REGION
});

const UserModel = require('../models/user');
const uniqueUserId = require('uuid/v1');

class UserRepository {
	constructor() {}
	addUser(user, callback) {

		officeRepository.getOffice(user.officeId, (error, Office) => {
			if (error) {
				callback(error);
			} else {
				//If Office Id already Exists then adding the builing to the that Office
				BuildingModel.getBuilding(user.buildingId, function (err, building) {
					if (err) {
						return callback(err);
					}
					//If Office Id and Building id matches then adding the builing to the that Office
					FloorModel.getFloor(user.floorId, function (err, floor) {
						if (err) {
							return callback(err);
						}
						//If Office Id, Building id and FloorId combination matches then adding the desk to the that floor
						DeskModel.getDesk(user.deskId, function (err, desk) {
							if (err) {
								return callback(err);
							}
							//While creating the User assinging the desk as well
							user.userId = uniqueUserId();
							UserModel.create(user, function (err, user) {
								if (err) {
									return callback(err);
								}
								return callback(null, user);
							});
						});
					});
				});
			}
		});
	}
	getUser(userId, callback) {
		UserModel.scan(Constants.USER_ID).eq(userId).exec(function (err, user) {
			if (err) {
				return callback(err);
			}
			return callback(null, user);
		});
	}
	updateUser(userId, user, callback) {
		//Update needs to be modified according to the changes that are going to be updated

		UserModel.update({
			'userId': userId
		}, {
			$PUT: {
				'address': user.address
			}
		}, function (err, user) {
			if (err) {
				return callback(err);
			}
			return callback(null, user);
		});
	}

	deleteUser(userId, callback) {
		UserModel.delete(userId, function (err, user) {
			if (err) {
				return callback(err);
			}
			return callback(null, user);
		});
	}
}

module.exports = UserRepository;