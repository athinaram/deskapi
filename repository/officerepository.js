'use strict';
/**
 * 
 */
var AWS = require("aws-sdk");
var dynamoose = require('dynamoose');
var Constants = require('../constants/constants');

dynamoose.AWS.config.update({
	region: Constants.REGION
});

const OfficeModel = require('../models/office');
const uniqueofficeId = require('uuid/v1');

const floorRepository = require('../repository/floorrepository');
const deskRepository = require('../repository/deskrepository');
const userRepository = require('../repository/userrepository');
const buildingRepository = require('../repository/buildingrepository');

class OfficeRepository {
	constructor() {

	}

	addOffice(office, callback) {
		let officeList = this.getOfficeByName(office.officeName);
		if (officeList && officeList.length < 1) {
			//Adding new office first time
			this.addNewOffice(office);
		} else {
			let existingOffice = false;
			officeList.forEach(element => {
				if (element.officeName === office.officeName && element.address === office.address) {
					existingOffice = true;
					break;
				}
			});
			if (!existingOffice) {
				//New Branch for the same office name but diffrent location
				this.addNewOffice(office);
			} else {
				callback('Office and Address combination already exist.');
			}

		}
	}

	// Add new Office
	addNewOffice(office) {
		office.officeId = uniqueofficeId();
		OfficeModel.create(office, function (err, office) {
			if (err) {
				callback(err);
			}
			callback(null, office);
		});
	}

	getOffice(officeId, callback) {
		OfficeModel.scan(Constants.OFFICE_ID).eq(officeId).exec(function (err, office) {
			if (err) {
				callback(err);
			}
			if (office && office.length > 0) {
				callback(null, office);
			} else {
				callback('Office doesnt exist.');
			}
		});
	}

	getOfficeByName(officeName, callback) {
		OfficeModel.scan(Constants.OFFICE_NAME).eq(officeName).exec(function (err, officeList) {
			if (err) {
				callback(err);
			}
			if (officeList && officeList.length > 0) {
				callback(null, officeList);
			} else {
				callback('Office Name doesnt exist.');
			}
		});
	}

	updateOffice(officeId, office, callback) {
		//Update needs to be modified according to the changes that are going to be updated

		OfficeModel.update({
			'officeId': officeId
		}, {
			$PUT: {
				//Correcting Office Name
				'officeName': office.officeName,
				'address': office.address,
				'buildings': office.buildings,
				'createdBy': office.createdBy,
				'updatedBy': office.updatedBy
			}
		}, function (err, office) {
			if (err) {
				return callback(err);
			}
			return callback(null, office);
		});
	}

	updateOfficeBuildingCount(officeId, increment, callback) {
		let officeList = this.getOffice(officeId);
		let office = officeList[0];
		let buildingCount = office.buildings;
		if (increment) {
			buildingCount++;
		} else {
			buildingCount--;
		}
		//Updating the building count when adding or deleting building for office
		OfficeModel.update({
			'officeId': officeId
		}, {
			$PUT: {
				'buildings': buildingCount
			}
		}, function (err, office) {
			if (err) {
				return callback(err);
			}
			return callback(null, office);
		});
	}

	deleteOffice(officeId, callback) {
		OfficeModel.delete(officeId, function (err, office) {
			if (err) {
				callback(err);
			}
			//Delete the whole Heirarchy
			buildingRepository.deleteBuildingsByOfficeId(officeId, function (err, buildings) {
				if (err) {
					callback(err);
				}
				if (buildings && buildings.length > 0) {
					buildings.forEach(building => {
						floorRepository.deleteFloorsByBuildingId(building.buildingId, function (err, floors) {
							if (err) {
								callback(err);
							}
							floors.forEach(floor => {
								deskRepository.deleteDesksByFloorId(floor.floorId, function (err, desk) {
									if (err) {
										callback(err);
									}
								});
							});
						});
					});
				}
			});
		});
	}
}

module.exports = OfficeRepository;