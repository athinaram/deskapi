'use strict';
/**
 * 
 */
var AWS = require("aws-sdk");
var dynamoose = require('dynamoose');
var Constants = require('../constants/constants');

dynamoose.AWS.config.update({
	region: Constants.REGION
});

const FloorModel = require('../models/floor');
const uniquefloorId = require('uuid/v1');

const buildingRepository = require('../repository/buildingrepository');


class FloorRepository {

	constructor() {}

	addFloor(floor, callback) {
		floor.floorId = uniquefloorId();
		FloorModel.create(floor, function (err, floor) {
			if (err) {
				callback(err);
			}
			let increment = true;
			buildingRepository.updateBuildingFloorsCount(floor.buildingId, increment);
			callback(null, floor);
		});
	}

	getFloor(floorId, callback) {
		FloorModel.scan(Constants.floor_ID).eq(floorId).exec(function (err, floor) {
			if (err) {
				callback(err);
			}
			callback(null, floor);
		});
	}
	updateFloor(floorId, floor, callback) {
		//Update needs to be modified according to the changes that are going to be updated

		FloorModel.update({
			'floorId': floorId
		}, {
			$PUT: {
				'address': floor.address,
				'buildingId': floor.buildingId,
				'officeId': floor.officeId,
				'desks': floor.desks,
				'updatedBy': floor.updatedBy
			}
		}, function (err, floor) {
			if (err) {
				callback(err);
			}
			callback(null, floor);
		});
	}

	updateFloorDesksCount(floorId, increment, callback) {
		let floorList = this.getFloor(floorId);
		let floor = floorList[0];
		let deskCount = floor.desks;
		if (increment) {
			deskCount++;
		} else {
			deskCount--;
		}
		//Updating the Desks count when adding or deleting desk in the floor
		FloorModel.update({
			'floorId': floorId
		}, {
			$PUT: {
				'desks': deskCount
			}
		}, function (err, floor) {
			if (err) {
				callback(err);
			}
			callback(null, floor);
		});
	}

	deleteFloor(floorId, callback) {
		FloorModel.delete(floorId, function (err, floor) {
			if (err) {
				callback(err);
			}
			let increment = false;
			buildingRepository.updateBuildingFloorsCount(floor.buildingId, increment);
			callback(null, floor);
		});
	}
}

module.exports = FloorRepository;