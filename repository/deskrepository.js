'use strict';
/**
 * 
 */
var AWS = require("aws-sdk");
var dynamoose = require('dynamoose');
var Constants = require('../constants/constants');

dynamoose.AWS.config.update({
	region: Constants.REGION
});

const DeskModel = require('../models/desk');
const uniquedeskId = require('uuid/v1');

const floorRepository = require('../repository/floorrepository');

class DeskRepository {
	constructor() {}

	addDesk(newdesk, callback) {
		this.getDesk(newdesk.deskId, (error, desk) => {
			if (error) {
				callback(error);
			} else {
				if (desk && desk.length < 1) {
					DeskModel.create(newdesk, function (err, addedDesk) {
						if (err) {
							return callback(err);
						}
						let increment = true;
						floorRepository.updateFloorDesksCount(addedDesk.floorId, increment);
						return callback(null, building);
					});
				}
			}
		});
	}

	getDesk(deskId, callback) {
		DeskModel.scan(Constants.desk_ID).eq(deskId).exec(function (err, desk) {
			if (err) {
				return callback(err);
			}
			return callback(null, desk);
		});
	}

	updateDesk(deskId, desk, callback) {
		//Update needs to be modified according to the changes that are going to be updated

		DeskModel.update({
			'deskId': deskId
		}, {
			$PUT: {
				'floorId': desk.floorId,
				'buildingId': desk.buildingId,
				'officeId': desk.officeId,
				'userId': desk.userId,
				'deskNumber': desk.deskNumber,
				'available': desk.available,
				'availabilityStartDate': desk.availabilityStartDate,
				'availabilityEndDate': desk.availabilityEndDate,
				'temporarilyAssigned': desk.temporarilyAssigned,
				'notes': desk.notes,
				'updatedBy': desk.updatedBy
			}
		}, function (err, desk) {
			if (err) {
				return callback(err);
			}
			return callback(null, desk);
		});
	}

	deleteDesk(deskId, callback) {
		DeskModel.delete(deskId, function (err, desk) {
			if (err) {
				return callback(err);
			}
			return callback(null, desk);
		});
	}
}

module.exports = DeskRepository;