/**
 * 
 * @author: Siva Kumar Athinaram
 */

/* global it, describe, before, after */
const LambdaTester = require('lambda-tester').noVersionCheck();
const buildingHandler = require('../handlers/buildinghandler');
const deskHandler = require('../handlers/deskhandler');
const floorHandler = require('../handlers/floorhandler');
const officeHandler = require('../handlers/officehandler');
const userHandler = require('../handlers/userhandler');
const assert = require('assert');

describe('Desk API', function () {

    
    /*  it('addOffice', function () {
         return LambdaTester(officeHandler.addOffice)
             .event({
                 pathParameters: {},
                 body: {
                     "officeName": "Toyota",
                     "address": "6565 Headquarter Drive",
                     "buildinds": "4",
                     "createdBy": "test",
                     "updatedBy": "test"
                 }
             })
             .expectResult((result) => {
                 console.log(result);
             });
     });
    it('getOffice', function () {
        return LambdaTester(officeHandler.getOffice)
            .event({
                pathParameters: {
                    officeId: "0990d800-0a11-11e9-bcd7"
                }
            })
            .expectResult((result) => {
                console.log(result);
            });
    });
     it('updateOffice', function () {
         return LambdaTester(officeHandler.updateOffice)
             .event({
                 pathParameters: {
                     officeId: "0990d800-0a11-11e9-bcd7-95b718a03dab"
                 },
                 body: {
                     "address": "1608 Monahan Drive"
                 }
             })
             .expectResult((result) => {
                 console.log(result);
             });
     });
     it('deleteOffice', function () {
         return LambdaTester(officeHandler.deleteOffice)
             .event({
                 pathParameters: {
                     officeId: "0990d800-0a11-11e9-bcd7-95b718a03dab"
                 }
             })
             .expectResult((result) => {
                 console.log(result);
             });
     }); 
     */
    it('addBuilding', function () {
        return LambdaTester(buildingHandler.addBuilding)
            .event({
                pathParameters: {},
                body: {
                    "officeId": "17711700-0a11-11e9-9c1f-dd74a6bd44e4",
                    "floors": "4",
                    "address": "1608 Monahan Drive",
                    "createdBy": "test",
                    "updatedBy": "test"
                }
            })
            .expectResult((result) => {
                console.log(result);
            });
    });

    // it('getOffice', function () {
    //     return LambdaTester(buildingHandler.getOffice)
    //         .event({
    //             pathParameters: {
    //                 officeId: "0990d800-0a11-11e9-bcd7-95b718a03dab"
    //             }
    //         })
    //         .expectResult((result) => {
    //             console.log(result);
    //         });
    // });
    // it('updateOffice', function () {
    //     return LambdaTester(buildingHandler.updateOffice)
    //         .event({
    //             pathParameters: {
    //                 officeId: "0990d800-0a11-11e9-bcd7-95b718a03dab"
    //             },
    //             body: {
    //                 "address": "1608 Monahan Drive"
    //             }
    //         })
    //         .expectResult((result) => {
    //             console.log(result);
    //         });
    // });
    // it('deleteOffice', function () {
    //     return LambdaTester(buildingHandler.deleteOffice)
    //         .event({
    //             pathParameters: {
    //                 officeId: "0990d800-0a11-11e9-bcd7-95b718a03dab"
    //             }
    //         })
    //         .expectResult((result) => {
    //             console.log(result);
    //         });
    // }); 


});