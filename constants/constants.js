'use strict';
/**
 * Constants for US English Language
 */
module.exports = {
    EMPTY_BODY : 'Event Body cannot be empty.',
    USER_ID : 'userId',
    EMPTY: ' cannot br empty.',
    OFFICE_ID: 'officeId',
    REGION: 'us-east-1',
    OFFICE_NAME: 'officeName'
};
